FROM java:8

LABEL maintainer="Sergiu Marsavela <smarsavela@dathena.io>"

# copy the current directory to the container as ./app
ADD ./target/scala-2.12/simple-api-scala-assembly-1.0.0.jar simple-api-scala-assembly-1.0.0.jar
# set the working directory in the container to ./app

EXPOSE 8080
CMD java -jar simple-api-scala-assembly-1.0.0.jar